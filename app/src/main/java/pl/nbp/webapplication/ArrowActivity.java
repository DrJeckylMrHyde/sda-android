package pl.nbp.webapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

abstract class ArrowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();
    }

    void setToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Kurs złota");

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        obsługa strzałki w górnym menu
        if (item.getItemId() == android.R.id.home) {
//            zamknięcie obecnej aktywności
            finish();
//            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
