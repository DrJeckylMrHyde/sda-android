package pl.nbp.webapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Table {

    @SerializedName("rates")
    private List<Rate> rates;

    public List<Rate> getRates() {

        return rates;
    }
}
