package pl.nbp.webapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.nbp.webapplicatio.R;
import pl.nbp.webapplication.models.Gold;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoldActivity extends ArrowActivity {
    ProgressBar progressBar;
    private List<Integer> days = Arrays.asList(
            10, 20, 30, 50, 90
    );

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gold);

//        metoda obsługująca listę rozwijaną
        setSpinner();
    }

    private void setSpinner() {
        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter<Integer> adapter =
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_spinner_item,
                        days
                );

        spinner.setAdapter(adapter);

//        dodanie reakcji na wybór elementu ze spinnera
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchData(days.get(position));
            }

//            sytuacja w której kliknięto na listę ale odkliknięto w puste pole
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void fetchData(Integer days) {
        Call<List<Gold>> call = NbpApi.getApi().getGold(days);
        call.enqueue(getCallback());
    }

    private Callback<List<Gold>> getCallback() {
        return new Callback<List<Gold>>() {
            @Override
            public void onResponse(Call<List<Gold>> call, Response<List<Gold>> response) {
//                progressBar.setVisibility(View.GONE);
                if(response.body() != null && !response.body().isEmpty()) {
                    showData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Gold>> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//
//                t.printStackTrace();
//                String message = t.getMessage();
//
//                TextView error = findViewById(R.id.error);
//                error.setText(message);
//                error.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showData(List<Gold> goldRates) {
        LineChart chart = findViewById(R.id.chart);

        chart.clear();

        ArrayList<Entry> values = new ArrayList<>();
        for (int position = 0; position < goldRates.size(); position++) {
//            biblioteka obsługująca punkty na wykresie
            Entry entry = new Entry(position,
                    goldRates.get(position).getPrice());
            values.add(entry);
        }

//        Klasa odpowiada za jedną linię na wykresie
//        za ich łączenie
        LineDataSet set = new LineDataSet(values, "Kursy złota");
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setColor(getResources().getColor(R.color.colorPrimaryDark));
        set.setFillColor(getResources().getColor(R.color.colorPrimary));

//        wypełnienie pod wykresem
        set.setDrawFilled(true);

//        czy rysować kółka na wartościach
        set.setDrawCircles(false);
        set.setLineWidth(6f);

        LineData data = new LineData(set);
        data.setValueTextSize(18f);

        chart.setData(data);

        chart.invalidate();
    }
}
