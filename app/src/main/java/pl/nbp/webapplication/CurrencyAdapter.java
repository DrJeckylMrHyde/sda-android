package pl.nbp.webapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.nbp.webapplicatio.R;
import pl.nbp.webapplication.models.Rate;

//dziedzimy po klasie która wie jak ładować listę na ekranie jak ruchome schody
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.Row> {

    private List<Rate> rates;

    public CurrencyAdapter(List<Rate> rates) {
        this.rates = rates;
    }

//    metoda obsługująca to ile obiektów wyświetli w danej chwili na ekranie
    @NonNull
    @Override
    public Row onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        roumie xml przerabia na zrozumiala dla siebie forme
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.currency_row, viewGroup,false);

        return new Row(view);
    }


//    metoda sluzaca wypelnieniu wiersza danym kursem
    @Override
    public void onBindViewHolder(@NonNull Row row, int i) {
//        i to pozycja
        Rate rate = rates.get(i);
        row.bind(rate);
    }

//    metoda zwracająca ilość obiektów
    @Override
    public int getItemCount() {

        return rates.size();
    }

//    klasa służąca za obsługę jednego pojedyńczego wiersza
    class Row extends RecyclerView.ViewHolder {

//        elementy składowe wiersza, pola w wierszu
        private TextView name;
        private TextView code;
        private TextView mid;

        public Row(@NonNull View itemView) {
            super(itemView);

            code = itemView.findViewById(R.id.code);
            name = itemView.findViewById(R.id.name);
            mid = itemView.findViewById(R.id.mid);
        }

        void bind(Rate rate) {
            code.setText(rate.getCode());
            name.setText(rate.getName());
            mid.setText(String.format("%f",rate.getMid()));
        }
    }
}
