package pl.nbp.webapplication;

import android.annotation.TargetApi;
import android.os.Build;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.nbp.webapplication.models.Gold;
import pl.nbp.webapplication.models.Table;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NbpApi {

    String BASE_URL = "http://api.nbp.pl/api/";

    @GET("exchangerates/tables/A")
    Call<List<Table>> getCurrencies();

    @GET("cenyzlota/last/{number}")
    Call<List<Gold>> getGold(@Path("number") int numberOfElements);

    @TargetApi(Build.VERSION_CODES.N)
    static NbpApi getApi(){

        return NbpApi.getRetrofit().create(NbpApi.class);
    }

    static Retrofit getRetrofit(){
//        sluży pobieraniu informacji ze strony internetowej
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();


        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
//                to powoduje, że otrzymuje listę obiektów a nie tekst
//                tzn tekst jest już przetworzony
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
