package pl.nbp.webapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import pl.nbp.webapplicatio.R;
import pl.nbp.webapplication.models.Rate;
import pl.nbp.webapplication.models.Table;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyActivity extends ArrowActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        pobiera zasoby z klasy R - resource
        setContentView(R.layout.activity_currency);

//        Pobranie danych
        fetchData();
        progressBar = findViewById(R.id.progress_bar);
    }

    private void fetchData() {
        Call<List<Table>> call = NbpApi.getApi().getCurrencies();
        call.enqueue(getCallback());
    }

    private Callback<List<Table>> getCallback() {
        return new Callback<List<Table>>() {
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {
                progressBar.setVisibility(View.GONE);

//                Sprawdzenie czy kod odpowiedzi jest poprawny
//                tzn. w zakresie 200-299
//                kody błędów odpowiedzi html
                if (response.isSuccessful()
                        && response.body() != null
                        && !response.body().isEmpty()) {

//                    wyświetlenie danych
                    showData(response.body().get(0));
                }
            }

            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);


                t.printStackTrace();
                String message = t.getMessage();

//                Wypełnienie i wyświetlenie błędu
                TextView error = findViewById(R.id.error);
                error.setText(message);
//                error.setText("Wystąpił błąd, spróbuj później.");
                error.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showData(Table table) {
//        Łącze z widokiem, dostaje dostęp do kontenera,
//        który ma dostęp do przewijalnej listy
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

//        Przekazujemy kursy do adaptera
        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        jest to opcja powodująca powstanie szarej kreski między kolejnymi liniami
//        można to również ustawić w layout
        DividerItemDecoration divider =
//                podajemy jak ulozone sa elementy
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        recyclerView.addItemDecoration(divider);
    }


}
