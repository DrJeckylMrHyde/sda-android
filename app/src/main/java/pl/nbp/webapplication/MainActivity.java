package pl.nbp.webapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import pl.nbp.webapplicatio.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ustawienia widoku aplikacji (z folderu layout)
        setContentView(R.layout.activity_main);

        setupUi();
    }

    private void setupUi() {
        Button first = findViewById(R.id.first_button);
        Button second = findViewById(R.id.second_button);

//        Dodanie listenera reagującego na "krótkie" kliknięcie
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        GoldActivity.class);

//                Otwarcie nowej aktywności
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
//
                        CurrencyActivity.class);

                startActivity(intent);
            }
        });


    }


}
